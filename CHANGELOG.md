# Changelog

## 2.2.0

### Updated

- Adopt kaniko 2.4.0

## 2.1.0

### Added

- pegs all container tag input defaults to the component version being published, but only if it finds them set to `default: _UPDATE_DURING_COMPONENT_PUBLISH_`
- Build prompt for forced version moved into component to simplify calling code.

## 1.4.0

### Added

- automatically detects and builds container with same version is there is a Dockerfile at the root of the project.
- v2.1.0 pegs all container tag input defaults to the component version being published, but only if it finds them set to `default: _UPDATE_DURING_COMPONENT_PUBLISH_`
- v2.1.0 Build prompt for forced version moved into component to simplify calling code.