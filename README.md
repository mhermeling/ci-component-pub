# CI Component Publishing Component

Publishes new components with the following features:
- validates that you have a project description
- validates that you have a project README.md
- automatically manages versions numbers
- automatically detects and builds container with same version is there is a Dockerfile at the root of the project.
- pegs all container tag input defaults to the component version being published, but only if it finds them set to `default: _UPDATE_DURING_COMPONENT_PUBLISH_`
- Build prompt for forced version moved into component to simplify calling code.

**NOTE:** 2024-05-07 GitLab does not yet support semver prereleases, so under certain circumstances a version conflict may occur. Subscribe to this issue to follow resolution of support for prereleases: [Add sorting option for `prerelease` for semver concern](https://gitlab.com/gitlab-org/gitlab/-/issues/441266)

## Usage

Builds a new version when you change the code.

Click "Run pipeline" to build the current code as a specific version number.

For automated versioning behavior of this component, see docs for [Ultimate Auto Semversioning](https://gitlab.com/explore/catalog/guided-explorations/ci-components/ultimate-auto-semversioning#usage)


You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: gitlab.com/components/ci-component-pub/release-ci-component@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

### Automatic Container Building

Frequently CI components will have an associated container. If this publishing component finds a Dockerfile, it uses the Kaniko CI Component to build a container with the same version as the component. If you force the version of the component build, the container will match. If your Dockerfile is in a non-standard place, you can configure the CI Variable `KANIKO_DOCKERFILE` to point to it as per the Kaniko CI Component instructions: https://gitlab.com/explore/catalog/guided-explorations/ci-components/kaniko

### Inputs and Configuration

See also docs for [Ultimate Auto Semversioning](https://gitlab.com/explore/catalog/guided-explorations/ci-components/ultimate-auto-semversioning#usage)

| Input | Default value |    Type     | Description |
| ----- | ------------- | ----------- | ----------- |
| `CI_DEBUG_TRACE` | N/A | CI Variable     | Causes verbose component output for debugging. Set for pipeline or specific job. |
| `FORCE_RELEASE_VERSION_TO` | N/A | CI Variable     | Use the Run pipeline button and fill in this prompted variable to force a new build with the version number you provide. |

### Debug Tracing

Set either CI_COMPONENT_TRACE or GitLab's global trace (CI_DEBUG_TRACE) to 'true' to get detailed debugging. More info: https://docs.gitlab.com/ee/ci/variables/#enable-debug-logging"

### Validation Test

If you simply include the component, the job log for a job called 'hello-world' should contain the logged text 'Hello CI Catalog World'

### Working Example Code Using This Component

- [Hello World Working Example Code](https://gitlab.com/guided-explorations/ci-components/working-code-examples/hello-world-component-test/)

## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components

## Attributions and Credits

<a href="https://www.flaticon.com/free-icons/hello-world" title="hello world icons">Hello world icons created by IconBaandar - Flaticon</a>